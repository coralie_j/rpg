package com.example.rpg.carte;

import com.example.rpg.destructible.Destructible;
import com.example.rpg.personnage.Personnage;

public class Carte {

    private String[][] map;

    public Carte(){
        this.map = new String[][]{
                {"", "", "", "", "", "", "", "", "", "", ""},
                {"", "", "", "", "", "", "", "", "", "", ""},
                {"", "", "", "", "", "", "", "", "", "", ""},
                {"", "", "", "", "", "", "", "", "", "", ""},
                {"", "", "", "", "", "", "", "", "", "", ""},
                {"", "", "", "", "", "", "", "", "", "", ""},
                {"", "", "", "", "", "", "", "", "", "", ""},
                {"", "", "", "", "", "", "", "", "", "", ""},
                {"", "", "", "", "", "", "", "", "", "", ""},
                {"", "", "", "", "", "", "", "", "", "", ""},
                {"", "", "", "", "", "", "", "", "", "", ""},
                {"", "", "", "", "", "", "", "", "", "", ""}
        };
    }


    public String[][] getMap() {
        return this.map;
    }

    public void videCase(int col, int lig){
        this.map[col][lig] = "";
    }

    /**
     * @param p Personnage
     * @return Retourne la position du personnage p donné en paramètre dans un tableau [col,lig]
     */

    public int[] getPersonnagePosition(Personnage p){
        String nom_perso = p.getNom();
        int[] dico_pos = new int[2];
        for (int i=0; i< this.map.length; i++){
            for (int j=0; j < this.map[0].length; j++){
                if (this.map[i][j].equals(nom_perso)){
                    dico_pos[0] = i;
                    dico_pos[1] = j;
                    break;
                }
            }
        }
        return dico_pos;
    }

    public void placePersonnage(Personnage p, int col, int lig){
        this.map[col][lig] = p.getNom();
    }

    /**
     * @param p Personnage à placer
     * Place un personnage de manière aléatoire dans une carte.
     */

    public void placePersonnage(Personnage p){

        int col = (int) (Math.random() * this.map.length);
        int lig = (int) (Math.random() * this.map[0].length);

        while (! this.map[col][lig].equals("")){
            col = (int) (Math.random() * this.map.length);
            lig = (int) (Math.random() * this.map[0].length);
        }

        this.map[col][lig] = p.getNom();
    }

    /**
     *  @param d Tableau de destructibles à placer
     *  Place les destructibles aléatoirement sur la carte
     */

    public void placeDestructibles(Destructible[] d){
        for (int i=0; i < d.length; i++){
            int col = (int) (Math.random() * this.map.length);
            int lig = (int) (Math.random() * this.map[0].length);

            while (! this.map[col][lig].equals("")){
                col = (int) (Math.random() * this.map.length);
                lig = (int) (Math.random() * this.map[0].length);
            }

            this.map[col][lig] = d[i].getStr_representant();
        }
    }

    /**
     * @param col entier correspondant à la colonne
     * @param lig entier correspondant à la ligne
     * @return Retourne le contenu de la case (col, lig)
     */

    public String getContenuCase(int col, int lig){
        return this.map[col][lig];
    }

    public int[] verify_destructible_around(int col, int lig){
        int col1 = col - 1; // case du bas
        int col2 = col + 1; // case du haut
        int lig1 = lig - 1; // case arrière
        int lig2 = lig + 1; // case avant

        int[] position = {-1, -1};

        // Vérification que les cases autour de (col, lig) soient dans la map

        if (col1 >= 0){
            if (! this.map[col1][lig].equals("")){
                position[0] = col1;
                position[1] = lig;
            }
        }

        if (col2 < this.map.length){
            if (! this.map[col2][lig].equals("")){
                position[0] = col2;
                position[1] = lig;
            }
        }

        if (lig1 >= 0){
            if (! this.map[col][lig1].equals("")){
                position[0] = col;
                position[1] = lig1;
            }
        }

        if (lig2 < this.map[0].length){
            if (! this.map[col][lig2].equals("")){
                position[0] = col;
                position[1] = lig2;
            }
        }

        return position;
    }

    public boolean estVide(int col, int lig){
        if (this.map[col][lig].equals("")) return true;
        return false;
    }

    /**
     * @return Affiche la carte où on se déplace.
     *
     */

    public void affiche(){
        String line = "";
        String coord = "\t";
        char lig_char = 'a';
        int lig_int  = (int) lig_char;
        for (int i=0; i < this.map.length; i++){

            line += (i + 1) + "\t";

            for (int j=0; j < this.map[0].length; j++){
                line += this.map[i][j] + "\t";
            }
            line += "\n";
        }

        for (int i=0; i < this.map[0].length; i++){
            coord += Character.toString(lig_char) + "\t";
            lig_int++;
            lig_char = (char) lig_int;
        }

        System.out.println(coord);
        System.out.println(line);
    }
}
