package com.example.rpg.partie;

import com.example.rpg.arme.Arme;
import com.example.rpg.arme.Bombe;
import com.example.rpg.arme.Couteau;
import com.example.rpg.arme.Epee;
import com.example.rpg.arme.Fusil;
import com.example.rpg.arme.GantDeBoxe;
import com.example.rpg.arme.Marteau;
import com.example.rpg.carte.Carte;
import com.example.rpg.destructible.Destructible;
import com.example.rpg.destructible.Obstacle;
import com.example.rpg.destructible.monstre.Caporal;
import com.example.rpg.destructible.monstre.KingMonstre;
import com.example.rpg.destructible.monstre.Monstre;
import com.example.rpg.destructible.monstre.Sergent;
import com.example.rpg.destructible.monstre.Soldat;
import com.example.rpg.magasinarme.MagasinArme;
import com.example.rpg.personnage.Personnage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Partie {

    private Personnage personnage;
    private Carte map;
    private Destructible[] destructibles;
    private MagasinArme magasinArme;

    public Partie(){}

    public Partie(Personnage p, Carte c, Destructible[] destructibles, MagasinArme magasinArme){
        this.personnage = p;
        this.map = c;
        this.destructibles = destructibles;
        this.magasinArme = magasinArme;
    }

    /**
     * Permet d'initialiser une partie de manière interactive
     * @return la partie initialisé
     */

    public Partie init(){
        Personnage p = this.creer_personnage();
        MagasinArme magasinArme = this.creer_magasin_arme();
        Carte map = new Carte();
        Destructible[] destructibles = this.creer_destructibles();
        map.placePersonnage(p);
        map.placeDestructibles(destructibles);
        return new Partie(p, map, destructibles, magasinArme);
    }

    /**
     * Retourne le personnage de la partie courante
     * @return Personnage de la partie courante
     */

    public Personnage getPersonnage() {
        return this.personnage;
    }

    /**
     * Retourne la map de la partie courante
     * @return Carte de la partie courante
     */

    public Carte getMap() {
        return this.map;
    }

    /**
     * Crée des destructibles de la carte
     * @return les destructibles de la partie
     */

    private Destructible[] creer_destructibles(){
        Destructible[] destructibles = new  Destructible[12];

        Obstacle o;
        Soldat soldat;
        Caporal caporal;
        Sergent sergent;
        KingMonstre kingMonstre = new KingMonstre(800, "KingM");

        destructibles[11] = kingMonstre;

        for (int i = 0; i < 4; i++){
            o = new Obstacle(100 * i, "OBS"+i);
            destructibles[i] = o;
        }

        for (int j = 4; j < 7; j++){
            soldat = new Soldat(400, "Soldat" + ( j - 3));
            destructibles[j] = soldat;
        }

        for (int k = 7; k < 9; k++){
            caporal = new Caporal(550, "Capo" + (k - 6));
            destructibles[k] = caporal;
        }

        for (int l = 9; l < 11; l++){
            sergent = new Sergent(650, "Sergent" + (l - 8));
            destructibles[l] = sergent;
        }

        return destructibles;
    }

    /**
     * Crée un magasin d'armes
     * @return un magasin d'armes
     */

    private MagasinArme creer_magasin_arme(){
        ArrayList<Arme> armes = new ArrayList<>();

        Couteau c = new Couteau(999.99, 700);
        Bombe b = new Bombe(500, 56);
        Epee e = new Epee( 7500, 10000);
        GantDeBoxe g = new GantDeBoxe(3000, 7000);
        Marteau m = new Marteau(550, 350);
        Epee e2 = new Epee(800,700);
        Marteau m2 = new Marteau(700, 800);
        GantDeBoxe g2 = new GantDeBoxe(600, 550);

        armes.add(c);
        armes.add(b);
        armes.add(e);
        armes.add(g);
        armes.add(m);
        armes.add(e2);
        armes.add(m2);
        armes.add(g2);

        return new MagasinArme(armes);
    }

    /**
     * Crée un personnage de manière interactive
     * @return le personnage crée
     */

    private Personnage creer_personnage(){
        System.out.println("Bonjour, pour commencer une partie, vous devez choisir un nom de personnage");
        Scanner scanner = new Scanner(System.in);
        String nom = scanner.next().trim();

        while (nom.isEmpty()){
            System.out.println("Vous ne pouvez pas avoir un nom vide. Veuillez réessayer");
            nom = scanner.next().trim();
        }

        System.out.println(nom + ", quel arme souhaitez-vous utiliser ? Vous pouvez tapez : couteau , epée, fusil, gant, marteau.");
        String type_arme = scanner.next().toLowerCase().trim();

        while ( (! type_arme.equals("couteau")) && (! type_arme.equals("epée")) && (! type_arme.equals("fusil")) && (! type_arme.equals("gant")) && !(type_arme.equals("marteau"))){
            System.out.println("L'arme selectionnée n'est pas disponible. Veuillez réessayer");
            type_arme = scanner.next().toLowerCase().trim();
        }

        Arme a = null;

        switch (type_arme){
            case "couteau":
                a = new Couteau(29.99, 200);
                break;
            case "epée":
                a = new Epee(35.99, 200);
                break;
            case "fusil":
                a = new Fusil(35.99, 200);
                break;
            case "gant":
                a = new GantDeBoxe(29.99, 200);
                break;
            case "marteau":
                a = new Marteau(40.0, 200);
                break;
        }

        return new Personnage(nom, a);
    }

    /**
     * Gère le menu principal de la partie
     */

    public void show_menu_principal(){

        this.map.affiche();
        this.launch_combat();

        System.out.println("1 - Se déplacer");
        System.out.println("2 - Aller au magasin d'arme");
        System.out.println("3 - Quitter la partie");

        Scanner sc = new Scanner(System.in);
        String choix = sc.next();

        while (( ! choix.equals("1")) && ( ! choix.equals("2")) && ( ! choix.equals("3"))){
            System.out.println("Choix invalide. Réessayez ! ");
            choix = sc.next();
        }
        while (choix.equals("2") || choix.equals("1")){
            switch (choix){
                case "1":
                    int[] position_personnage = this.saisie_deplacement();
                    this.map.affiche();
                    this.launch_combat(position_personnage);
                    break;

                case "2":
                    this.magasinArme.exposer();
                    this.show_menu_magasin();
                    break;
            }

            System.out.println("1 - Se déplacer");
            System.out.println("2 - Aller au magasin d'arme");
            System.out.println("3 - Quitter la partie");
            choix = sc.next();

            while (( ! choix.equals("1")) && ( ! choix.equals("2")) && ( ! choix.equals("3"))){
                System.out.println("Choix invalide. Réessayez ! ");
                choix = sc.next();
            }
        }

        if (choix.equals("3")){
            System.out.println("Vous avez quitté la partie avec " + this.personnage.getPointsVie() + " points de vie et " + this.personnage.getNbVies() + " vies");
            System.out.println("Votre niveau est : " + this.personnage.getNiveau());
            System.exit(0);
        }
    }

    /**
     * Vérifie si un joueur est à côté d'un joueur, si c'est le cas,
     * lance le menu de combat.
     * @param coord_position_personnage tableau avec les coordonnées de la position du joueur
     */

    public void launch_combat(int[] coord_position_personnage){

        int[] position_destructible = this.map.verify_destructible_around(
                coord_position_personnage[0],
                coord_position_personnage[1]
        );

        if (position_destructible[0] != -1){
            this.show_menu_combat(position_destructible);
        }
    }

    /**
     * Au début de la partie, vérifie si le joueur est à côté d'un objet de type Destructible
     */

    public void launch_combat(){

        int[] position_personnage = this.map.getPersonnagePosition(this.personnage);
        int[] position_destructible = this.map.verify_destructible_around(
                position_personnage[0],
                position_personnage[1]
        );

        if (position_destructible[0] != -1) {
            this.show_menu_combat(position_destructible);
        }
    }

    /**
     * Gère le menu de combat
     * @param position_destructible position des destructibles
     *
     */

    public void show_menu_combat(int[] position_destructible){
        System.out.println("1 - Se déplacer");
        System.out.println("2 - Combattre");
        System.out.println("3 - Aller au magasin d'arme");

        Scanner sc = new Scanner(System.in);
        String choix = sc.next();

        switch (choix){
            case "1":
                int[] position_personnage = this.saisie_deplacement();
                this.map.affiche();
                break;
            case "2":
                String nom_monstre = this.map.getContenuCase(
                        position_destructible[0],
                        position_destructible[1]
                );

                Destructible d = this.getDestructible(nom_monstre);
                int i = 0;
                while (d.getNbPointsVie() > 0 && this.personnage.getPointsVie() > 0){
                    if (d.getClass() == Obstacle.class){
                        float degat_inflige = this.personnage.getArme().attaquer(d);
                        System.out.println("Vous avez infligé " + degat_inflige + " de dégat");
                    }
                    else {
                        if (i%2 == 0) {
                            float degat_inflige = this.personnage.getArme().attaquer(d);
                            System.out.println("Vous avez infligé " + degat_inflige + " de dégat");
                        }
                        else ((Monstre) d).attaquer(this.personnage);
                        i++;
                    }
                }
                if (d.getClass() == Obstacle.class) System.out.println("Obstacle détruit");

                if (this.personnage.getNbVies() <= 0 ) {
                    System.out.println("Perdu ! Vous n'avez plus de vie.");
                    System.exit(0);
                }
                else
                    System.out.println("Il vous reste " + this.personnage.getPointsVie() + " points de vie et " + this.personnage.getNbVies() + " vie(s)");

                if (d.getNbPointsVie() <= 0) {
                    this.map.videCase(position_destructible[0], position_destructible[1]);
                    this.personnage.augmenteDestructiblesAbattus();
                    int niveau_perso = this.personnage.getNiveau();
                    if (this.personnage.getDestructibles_abattus() == Personnage.destructibles_a_abattre[niveau_perso - 1]){
                        this.personnage.augmenteNiveau();
                    }
                }
                this.show_menu_principal();
                break;
            case "3":
                this.magasinArme.exposer();
                this.show_menu_magasin();
                break;
        }
    }

    /**
     * Gère le menu du magasin
     */

    public void show_menu_magasin(){
        System.out.println("1 - Acheter une arme");
        System.out.println("2 - Vendre une arme");
        System.out.println("3 - Partir");

        Scanner sc = new Scanner(System.in);
        String choix = sc.next();

        while ((! choix.equals("1")) && (! choix.equals("2")) &&  ! choix.equals("3")){
            System.out.println("Choix invalide. Veuillez réessayer");
            choix = sc.next();
        }

        while (choix.equals("2") || choix.equals("1")){
            switch (choix) {
                case "1":
                    Arme arme = this.manage_selection_arme();
                    this.magasinArme.vendre(arme, this.personnage);
                    System.out.println(this.personnage);
                    break;
                case "2":
                    this.magasinArme.ajouterArme(this.personnage.getArme());
                    this.personnage.vendreArme();
                    System.out.println(this.personnage);
                    break;
            }
            this.magasinArme.exposer();
            System.out.println("Vous pouvez saisir : ");
            System.out.println("1 - Acheter une arme");
            System.out.println("2 - Vendre une arme");
            System.out.println("3 - Partir");
            choix = sc.next();
        }


        while (this.personnage.getArme() == null){
            System.out.println("Vous ne pouvez pas quitter le magasin sans arme. Choisissez en une parmi celles exposés");
            Arme arme = this.manage_selection_arme();
            this.magasinArme.vendre(arme, this.personnage);
            System.out.println(this.personnage);
        }

        if (choix.equals("3")){
            System.out.println("Vous avez quitté le magasin");
            this.show_menu_principal();
        }
    }

    /**
     * Gère la selection d'arme lors d'un achat d'arme
     * @return Retourne l'arme selectionnée
     */

    public Arme manage_selection_arme() {
        Scanner sc = new Scanner(System.in);
        int indice_arme = -78;

        while (indice_arme < -1 || indice_arme > this.magasinArme.getNbArmes()){
            System.out.println("Selectionnez une arme via son indice : ");
            indice_arme = sc.nextInt() - 1;
        }

        return this.magasinArme.getArme(indice_arme);
    }

    /**
     * @param representation nom du monstre
     * @return Retourne l'objet de type Destructible qui porte le nom passé en paramètre
     */

    public Destructible getDestructible(String representation){
        for (int i=0; i < this.destructibles.length; i++){
            if (this.destructibles[i].getStr_representant().equals(representation)){
                return this.destructibles[i];
            }
        }
        return null;
    }

    /**
     * Permet de saisir le déplacement
     * @return Retourne la nouvelle position du joueur
     */

    public int[] saisie_deplacement(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Saisissez la case où vous souhaitez vous déplacer comme : f1");

        int lig_init = (int) 'a';
        String case_deplacement = sc.next().toLowerCase().trim();

        char lig_char = case_deplacement.charAt(0);
        int lig = (int) lig_char - lig_init;
        String col_str = case_deplacement.substring(1);
        int col = Integer.parseInt(col_str) - 1;

        int nb_col = this.map.getMap().length;
        int nb_lig = this.map.getMap()[0].length;

        // Vérification que la case est bien dans la map

        while (col > nb_col || lig > nb_lig){
            System.out.println("Case invalide. Veuillez réessayer");
            sc = new Scanner(System.in);
            case_deplacement = sc.next().toLowerCase().trim();
            lig_char = case_deplacement.charAt(0);
            lig = (int) lig_char - lig_init;
            col_str = case_deplacement.substring(1);
            col = Integer.parseInt(col_str) - 1;
        }

        // Vérification que la case est inoccupée

        while (! this.map.estVide(col, lig)){
            System.out.println("La case saisie est déjà occupée. Veuillez réessayer");
            sc = new Scanner(System.in);
            case_deplacement = sc.next().toLowerCase().trim();
            lig_char = case_deplacement.charAt(0);
            lig = (int) lig_char - lig_init;
            col_str = case_deplacement.substring(1);
            col = Integer.parseInt(col_str) - 1;
        }

        int[] position_anterieure = this.map.getPersonnagePosition(this.personnage);
        this.map.videCase(position_anterieure[0], position_anterieure[1]);
        this.map.placePersonnage(this.personnage, col, lig);
        return new int[]{col, lig};
    }

    @Override
    public String toString() {
        return "Partie{" +
                "personnage=" + personnage +
                ", map=" + map +
                ", destructibles=" + Arrays.toString(destructibles) +
                ", magasinArme=" + magasinArme +
                '}';
    }
}
