package com.example.rpg.destructible;

public class Destructible {

    public int nbPointsVie;
    public String str_representant;

    public Destructible(int nbPointsVie, String nom){
        this.nbPointsVie = nbPointsVie;
        this.str_representant = nom;
    }

    /**
     * Retourne les points de vie d'un Destructible
     * @return le nombre de points de vie
     */

    public int getNbPointsVie(){
        return this.nbPointsVie;
    }

    /**
     * Retourne la chaine qui réprésente un desctructible
     * @return chaine
     */

    public String getStr_representant() {
        return str_representant;
    }

    /**
     * Permet de diminuer les points de vie que possède le monstre par le paramètre impact_coup
     * @param impact_coup impact du coup sur le destructible
     */

    public void diminuerPointsVie(float impact_coup){
        this.nbPointsVie -= impact_coup;
    }

    @Override
    public String toString() {
        return "Destructible{" +
                "nbPointsVie=" + nbPointsVie +
                ", str_representant='" + str_representant + '\'' +
                '}';
    }
}
