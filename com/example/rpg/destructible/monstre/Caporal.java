package com.example.rpg.destructible.monstre;

import com.example.rpg.personnage.Personnage;

public class Caporal extends Monstre {

    private static int puissance_attaque = 350;

    public Caporal(int nbPointsVie, String nom) {
        super(nbPointsVie, nom);
    }

    @Override
    public void attaquer(Personnage p) {
        int degat_inflige = (int) (Caporal.puissance_attaque * 0.4);
        p.diminuerPointsVie(degat_inflige);
    }
}
