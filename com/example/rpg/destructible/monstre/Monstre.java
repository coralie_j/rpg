package com.example.rpg.destructible.monstre;

import com.example.rpg.personnage.Personnage;
import com.example.rpg.destructible.Destructible;

public abstract class Monstre extends Destructible {

    public Monstre(int nbPointsVie, String nom) {
        super(nbPointsVie, nom);
    }

    /**
     * Permet d'attaquer le personnage passé en paramètre
     * @param p Personnage à attaquer
     */

    public abstract void attaquer(Personnage p);

}
