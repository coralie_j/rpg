package com.example.rpg.destructible.monstre;

import com.example.rpg.personnage.Personnage;

public class KingMonstre extends Monstre {

    public static int puissance_attaque = 700;

    public KingMonstre(int nbPointsVie, String nom) {
        super(nbPointsVie, nom);
    }

    @Override
    public void attaquer(Personnage p) {
        int degat_inflige = (int) (KingMonstre.puissance_attaque * 0.9);
        p.diminuerPointsVie(degat_inflige);
    }

}
