package com.example.rpg.destructible.monstre;

import com.example.rpg.personnage.Personnage;

public class Soldat extends Monstre {

    public static int puissance_attaque = 300;

    public Soldat(int nbPointsVie, String nom) {
        super(nbPointsVie, nom);
    }

    @Override
    public void attaquer(Personnage p) {
        int degat_inflige = (int) (Soldat.puissance_attaque * 0.4);
        p.diminuerPointsVie(degat_inflige);
    }
}
