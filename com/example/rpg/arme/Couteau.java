package com.example.rpg.arme;

public class Couteau extends Arme {

    public Couteau(double prix, int puissance){
        super(prix, puissance);
    }

    @Override
    public String ascii_art(){
        return " ___________ ____________  \n" +
                "|           )._______.-'\n" +
                "`----------' ";
    }

    public static void main(String[] args){
        Couteau c = new Couteau(20.0, 200);
        System.out.println(c.ascii_art());
    }
}
