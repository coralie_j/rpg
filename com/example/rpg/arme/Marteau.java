package com.example.rpg.arme;

public class Marteau extends Arme {

    public Marteau(double prix, int puissance) {
        super(prix, puissance);
    }

    @Override
    public String ascii_art() {
        return " ______\n" +
                "|_,.,--\\\n" +
                "   ||\n" +
                "   ||\n" +
                "   ##\n" +
                "   ##";
    }

    public static void main(String[] args){
        Marteau m = new Marteau(45.45, 420);
        System.out.println(m.ascii_art());
    }
}
