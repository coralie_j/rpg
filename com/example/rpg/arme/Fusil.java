package com.example.rpg.arme;

public class Fusil extends Arme {

    public Fusil(double prix, int puissance) {
        super(prix, puissance);
    }

    @Override
    public String ascii_art() {
        return "      __,_____\n" +
                "    / __.==--\"\n" +
                "    /#(-'\n" +
                "    '-'\n";
    }
}
