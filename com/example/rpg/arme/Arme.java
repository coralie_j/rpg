package com.example.rpg.arme;

import com.example.rpg.destructible.Destructible;

public abstract class Arme {

    private double prix;
    private int puissance;

    public Arme(double prix, int puissance){
        this.prix = prix;
        this.puissance = puissance;
    }

    /**
     * Retourne le prix de l'arme courante
     * @return le prix de l'arme courante
     */

    public double getPrix(){
        return this.prix;
    }

    /** Retourne la puissance de l'arme courante
     * @return la puissance de l'arme courante
     */

    public int getPuissance(){
        return this.puissance;
    }

    /**
     * Inflige des dégats à un objet de type Destructible
     * @param d Destructible à attaquer
     * @return Retourne les dégats infligés
     */

    public float attaquer(Destructible d){
        float degat = (float) 0.4 * this.puissance;
        d.diminuerPointsVie(degat);
        return degat;
    }

    /**
     * Augmente la puissance de l'arme courante par le nombre de dégats suppélementaires
     * @param degat_suppl dégats supplémentaires
     */

    public void augmentePuissance(int degat_suppl){
        this.puissance += degat_suppl;
    }

    /**
     * Augmente le prix d'une arme
     */

    public void augmentePrix(){
        this.prix = this.puissance * 0.7;
    }

    /**
     * Permet d'afficher la photo de description de l'arme
     * @return Dessin d'affichage
     */

    public abstract String ascii_art();

    @Override
    public String toString() {
        return "Arme{" +
                "prix=" + this.prix +
                ", puissance=" + this.puissance +
                '}';
    }
}
