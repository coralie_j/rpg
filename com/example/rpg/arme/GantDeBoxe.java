package com.example.rpg.arme;

public class GantDeBoxe extends Arme {

    public GantDeBoxe(double prix, int puissance){
        super(prix, puissance);
    }

    @Override
    public String ascii_art() {
        return "  ))))(\n" +
                " /     \\\n" +
                "|       \\    )((((\n" +
                "|    |\\_)   /     \\  \n" +
                " \\___/     / ^     \\\n" +
                "          (_/ |    |\n" +
                "              \\___/";
    }

    public static void main(String[] args){
        GantDeBoxe g = new GantDeBoxe(50.50, 400);
        System.out.println(g.ascii_art());
    }
}
