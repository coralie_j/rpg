package com.example.rpg.arme;

public class Bombe extends Arme {

    public Bombe(double prix, int puissance){
        super(prix, puissance);
    }

    @Override
    public String ascii_art() {
        return "        ,--.!,\n" +
                "     __/ -*-\n" +
                "   ,d08b. '|`\n" +
                "   0088MM     \n" +
                "   '9MMP' ";
    }
}
