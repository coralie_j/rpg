package com.example.rpg.arme;

public class Epee extends Arme {

    public Epee(double prix, int puissance){
        super(prix, puissance);
    }

    public String ascii_art() {
        return "      0         \n" +
                "      |         \n" +
                "0{XXX}+========>\n" +
                "      |         \n" +
                "      0         \n";
    }

}
