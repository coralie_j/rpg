package com.example.rpg.magasinarme;

import com.example.rpg.arme.Arme;
import com.example.rpg.personnage.Personnage;

import java.util.ArrayList;

public class MagasinArme {

    private ArrayList<Arme> armes;

    public MagasinArme(ArrayList<Arme> armes){
        this.armes = armes;
    }

    /**
     * Affiche les armes disponibles
     */

    public void exposer(){

        Arme a;

        for (int i=0; i < this.armes.size(); i++){
            a = this.armes.get(i);
            System.out.println(a.ascii_art());
            System.out.println("\n" + (i+1) + ") Prix : " + a.getPrix() + "$ Puissance : " + a.getPuissance());
        }
    }

    /**
     * Permet la transaction de vente d'une arme. Si le budget d'un personnage n'est pas
     * suffisant pour acheter une arme, un message sera affiché.
     * @param a arme à vendre
     * @param p personnage souhaitant acheter une arme
     */

    public void vendre(Arme a, Personnage p){
        if (p.getMonnaie() > a.getPrix()) {
            this.armes.remove(a);
            p.acheterArme(a);
        }
        else System.out.println("Budget insuffisant pour acheter cet arme");
    }

    /**
     * Ajoute une arme dans le lot d'armes disponibles
     * @param a Arme à ajouter
     */

    public void ajouterArme(Arme a){
        this.armes.add(a);
    }

    /**
     * Retourne l'arme à l'indice i
     * @param i indice de l'arme choisie
     * @return Arme choisie
     */

    public Arme getArme(int i){
        return this.armes.get(i);
    }

    /** Retourne le nombre d'armes disponibles
     * @return le nombre d'armes disponibles
     */

    public int getNbArmes(){
        return this.armes.size();
    }

    /**
     * Retourne les armes disponibles
     * @return armes disponibles
     */

    public ArrayList<Arme> getArmes() {
        return this.armes;
    }

    @Override
    public String toString() {
        return "MagasinArme{" +
                "armes=" + armes +
                '}';
    }
}
