package com.example.rpg.personnage;

import com.example.rpg.arme.Arme;

public class Personnage {

    private double monnaie;
    private int nbVies;
    private int niveau;
    private int PointsVie;
    private int destructibles_abattus;
    private String nom;
    private Arme arme;

    public static int[] destructibles_a_abattre = { 1, 2, 4, 6, 8, 10, 11 };

    public Personnage(String nom, Arme a){
        this.nom = nom;
        this.arme = a;
        this.niveau = 1;
        this.PointsVie = 500;
        this.destructibles_abattus = 0;
        this.monnaie = 600;
        this.nbVies = 3;
    }

    /**
     * Retourne l'argent du personnage courant
     * @return monnaie
     */

    public double getMonnaie() {
        return this.monnaie;
    }

    /**
    * Retourne le nombre de vie du personnage courant
    * @return vie
    */

    public int getNbVies() {
        return this.nbVies;
    }

    /**
     * Retourne le nombre de points de vie du personnage courant
     * @return points de vie
     */

    public int getPointsVie() {
        return this.PointsVie;
    }

    /**
     * Retourne le niveau du personnage courant
     * @return niveau
     */

    public int getNiveau(){
        return this.niveau;
    }

    /**
     * Retourne le nom du personnage courant
     * @return nom
     */

    public String getNom() {
        return this.nom;
    }

    /**
     * Retourne l'arme du personnage courant
     * @return Arme
     */

    public Arme getArme() {
        return this.arme;
    }

    /**
     * Retourne le nombre de destructibles abbatus par le personnage courant
     * @return nombre de destructibles abbatus
     */

    public int getDestructibles_abattus() {
        return this.destructibles_abattus;
    }

    /**
     * Permet au personnage de vendre son arme
     */

    public void vendreArme(){
        this.arme = null;
    }

    /**
     * @param a : arme voulu
     * Permet au joueur d'acheter une arme
     */

    public void acheterArme(Arme a){
        this.monnaie -= a.getPrix();
        this.arme = a;
    }

    /**
     * Diminue le nombre de points de vie q'un joueur par le paramètre "dégar".
     * @param degat Dégat infligé
     */

    public void diminuerPointsVie(int degat){
        this.PointsVie-=degat;
        if (this.PointsVie <= 0 && this.nbVies > 0) {
            this.nbVies--;
            System.out.println("Vous avez perdu une vie . Il vous reste " + this.nbVies + " vies");
            this.PointsVie = this.niveau == 1 ? 500 : 500 + (100 * this.niveau);
        }
    }

    /**
     * Augmente le niveau d'un personnage, ses points de vie et augmente la puissance
     * d'attaque de son arme.
     */

    public void augmenteNiveau(){
        this.niveau++;
        this.PointsVie += 100 * this.niveau;
        this.arme.augmentePuissance(this.niveau/10 * 600);
    }

    /**
     * Augmente le nombre de destructibles abbatus par le joueur
     */

    public void augmenteDestructiblesAbattus(){
        this.destructibles_abattus++;
    }

    @Override
    public String toString() {
        return "Personnage{" +
                "monnaie=" + monnaie +
                ", nbVies=" + nbVies +
                ", niveau=" + niveau +
                ", PointsVie=" + PointsVie +
                ", nom='" + nom + '\'' +
                ", arme=" + arme +
                '}';
    }
}
